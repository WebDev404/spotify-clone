import type { NextPage } from 'next'
import Head from 'next/head'
import Header from '../components/Header'
import Sidebar from '../components/Sidebar'

const Home = () => {
  return (
    <div className='grow'>
      <Head>
        <title>Spotify Clone</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

     Main Content
    </div>
  )
}

export default Home

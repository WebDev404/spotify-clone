import Image from 'next/image';

const Player = () => {
	const performerImg = 'https://hips.hearstapps.com/hmg-prod/images/girlontherise-template-color-1508350042.png?crop=0.501xw:1.00xh;0.363xw,0&resize=640:*';

  return (
	  <div className="w-full fixed bottom-0 left-0 z-10 border-t border-[#2C2634] rounded-t-[50px] py-5">
		  <div className="max-w-7xl mx-auto px-3 flex items-start justify-between relative">
			  <Image className='rounded-lg block' width='60' height={60} src={performerImg} alt='Performer Image' />

			  <div>
					<p>Billie Eilish</p>
					<p>Album</p>
			  </div>

			  <div>
				Player
			  </div>
		</div>
	 </div>
  )
}

export default Player

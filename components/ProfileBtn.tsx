import Image from 'next/image';
import profileImg from '../assets/img/profile.jpg';

const ProfileBtn = () => {
  return (
	 <button className='w-16 h-16 rounded-full overflow-hidden relative'>
		  <Image className='w-full h-full object-cover' src={profileImg} alt='Profile image' />
	 </button>
  )
}

export default ProfileBtn

import Header from "./Header"
import Player from "./Player";
import Sidebar from "./Sidebar"

type DashboardLayoutProps = {
  children: React.ReactNode,
};

const Layout = ({ children }: DashboardLayoutProps) => {
  return (
	 <div>
		<Header />

		  <main className='max-w-7xl mx-auto px-3 flex items-start justify-between mt-10 gap-x-8'>
			 <Sidebar /> 

			  {children}
		  </main>

		  <Player />
	 </div>
  )
}

export default Layout

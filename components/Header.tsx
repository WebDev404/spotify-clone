import { BellIcon, ChevronLeftIcon, ChevronRightIcon, MagnifyingGlassIcon } from "@heroicons/react/20/solid";
import ProfileBtn from "./ProfileBtn";

const Header = () => {
	return (
		<header className="flex items-center justify-between max-w-7xl mx-auto px-3 py-4">
			<div className="flex items-center">
				<ProfileBtn />
				<div className="flex items-center ml-12 mr-6">
					<button><ChevronLeftIcon className="header-arrow" /></button>
					<button><ChevronRightIcon className="header-arrow" /></button>
				</div>

				<div className="flex items-center rounded-full border border-[#46494F] py-2 px-4 w-96">
					<MagnifyingGlassIcon className="w-6 h-6 mr-2 text-[#f7f7f7]" />
					<input className="bg-transparent outline-none shadow-none text-[#f7f7f7] placeholder:text-[#46494F] text-lg w-full" type="text" placeholder="Search for artists, songs and..." />
				</div>
			</div>

			<button className="w-12 h-12 flex items-center justify-center rounded-full bg-[#343511]">
				<BellIcon className="w-7 h-7 text-white" />
			</button>
		</header>
	)
}

export default Header

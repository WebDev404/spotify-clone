import {AdjustmentsHorizontalIcon, HomeIcon, MusicalNoteIcon} from "@heroicons/react/20/solid"
import Link from "next/link"
import {RiPlayListFill} from "react-icons/ri";
import {FcMusic} from "react-icons/fc";

const Sidebar = () => {
	return (
		<aside className="flex flex-col gap-y-5">
			<Link href='/'><HomeIcon className={'w-8 h-8'} /></Link>
			<Link href='/music'><FcMusic className={'color-white w-8 h-8'} /></Link>
			<Link href='/playlist'><RiPlayListFill className={'color-white w-8 h-8'} /></Link>
			<Link href='/settings'><AdjustmentsHorizontalIcon className="w-8 h-8"/></Link>
		</aside>
	)
}

export default Sidebar
